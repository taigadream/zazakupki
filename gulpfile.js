'use strict';

/*jshint scripturl:true*/

var gulp = require('gulp'),
    bs = require('browser-sync').create(),
    loadTasks = require('./tasks/_load-tasks');

var env = process.env.NODE_ENV;
var options = {};

options.isDev = !env || env.trim().toLowerCase() === 'dev' || env.trim().toLowerCase() === 'development';
options.isProd = !options.isDev;

options.vendorPath = "build/assets";

loadTasks(options);

gulp.task('assets', gulp.parallel(
    'assets:images',
    'assets:sass',
    'assets:fonts'
));

gulp.task('vendor', gulp.parallel(
    'vendor:scripts'
));

gulp.task('javascript', gulp.series(
    'javascript:build'
));

gulp.task('html', gulp.parallel(
    'html:index'
));

gulp.task('watch', function () {
    gulp.watch('src/assets/images/**/*', gulp.series('assets:images'));
    gulp.watch('src/assets/sass/**/*', gulp.series('assets:sass'));
    gulp.watch('src/assets/fonts/**/*', gulp.series('assets:fonts'));
    gulp.watch('src/assets/js/**/*.{js}', gulp.series('javascript'));
    gulp.watch('src/*.html', gulp.series('html:index'));

    process.WATCHING = true;
});

gulp.task('browsersync', function () {
    bs.init({
        server: {
            baseDir: './build'
        },
        ghostMode: false,
        https: false,
        startPath: "/"
    });

    gulp.watch('build/**/*.{js,json,html}').on('change', bs.reload);
    gulp.watch('build/**/*.css').on('change', bs.reload);
});

gulp.task('rebuild', gulp.series(
    'clean',
    gulp.parallel('assets', 'html', 'vendor')
));

gulp.task('default', gulp.series(
    'rebuild',
    gulp.parallel('watch', 'browsersync')
));

gulp.task('release', gulp.series(
    'rebuild'
));