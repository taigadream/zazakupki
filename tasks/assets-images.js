'use strict';

var gulp = require('gulp'),
	changed = require('gulp-changed'),
	errorHandler = require('./_error-handler');

module.exports = function (name) {
	return gulp.src('src/assets/images/**/*')
		.pipe(errorHandler(name))
		.pipe(changed('build/assets/images'))
		.pipe(gulp.dest('build/assets/images'));
};