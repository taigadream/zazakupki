'use strict';

var plumber = require('gulp-plumber'),
	notify = require('gulp-notify')

module.exports = function (title, continueSequence) {
	return plumber({
		errorHandler: function (err) {
			if (!process.WATCHING) {
				console.error(err.message);
				process.exit(1);
			}

			notify.onError({
				title: title,
				message: 'Error: <%= error.message %>',
				sound: 'Beep'
			})(err || Error('Что-то пошло не так'));

			if (continueSequence !== true) {
				this.emit('end');
			}
		}
	});
};