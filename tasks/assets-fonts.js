'use strict';

var gulp = require('gulp'),
    gulpIf = require('gulp-if'),
    concat = require('gulp-concat'),
    errorHandler = require('./_error-handler');

module.exports = function (name) {
    return gulp.src([
        // 'src/assets/fonts/roboto/*'
        // 'src/assets/fonts/robotocondenced/*',
        'src/assets/fonts/zcz-icons/*'
    ])
    .pipe(errorHandler(name))
    .pipe(gulpIf(/\.css$/, concat('fonts.css')))
    .pipe(gulp.dest('build/assets/fonts'));
};