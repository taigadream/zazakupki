'use strict';

var gulp = require('gulp'),
	streamify = require('gulp-streamify'),
	preprocess = require('gulp-preprocess'),
	errorHandler = require('./_error-handler');

module.exports = function (name, options) {
	return gulp.src(['src/*.html'])
		.pipe(errorHandler(name))
		.pipe(streamify(preprocess({ context: { DEBUG: options.isDev } })))
		.pipe(gulp.dest('build'));
};