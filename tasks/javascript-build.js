'use strict';

var gulp = require('gulp'),
    gulpIf = require('gulp-if'),
    uglify = require('gulp-uglify'),
    streamify = require('gulp-streamify'),
    preprocess = require('gulp-preprocess'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    // amdOptimize = require("amd-optimize"),
    errorHandler = require('./_error-handler');

module.exports = function (name, options) {
    return gulp.src("src/assets/js/**/*.{js}")
        .pipe(errorHandler(name))
        .pipe(streamify(preprocess({ context: { DEBUG: options.isDev } })))
        // .pipe(amdOptimize("kendo.mailclient.core"))
        .pipe(concat("scripts.js"))
        .pipe(gulpIf(options.isProd, streamify(uglify())))
        .pipe(gulpIf(options.isProd, rename({ suffix: ".min" })))
        .pipe(gulpIf(options.isDev, streamify(uglify({
            mangle: false,
            compress: false,
            output: {
                beautify: true
            }
        }))))
        .pipe(gulp.dest('build/js'));
};