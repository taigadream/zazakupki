'use strict';

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    errorHandler = require('./_error-handler');

var files = {
    dev: [
        'bower_components/jquery/dist/jquery.js'
    ],
    prod: [
        'bower_components/jquery/dist/jquery.min.js'
    ]
};

module.exports = function (name, options) {
    return gulp.src(options.isDev ? files.dev : files.prod)
        .pipe(errorHandler(name))
        //.pipe(gulpIf(options.isDev, sourcemaps.init()))
        .pipe(concat('vendor.js'))
        //.pipe(gulpIf(options.isDev, sourcemaps.write()))
        .pipe(gulp.dest(options.vendorPath + '/js'));
};