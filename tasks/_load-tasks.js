'use strict';

var fs = require('fs'),
	path = require('path'),
	loadTask = require('./_load-task');

module.exports = function (options, reg) {
	reg = reg || /.*/;

	fs.readdirSync('tasks').filter(function (name) {
		return !/^_load-task(s)?\.js$/.test(name) && reg.test(name);
	}).forEach(function (name) {
		var taskName = path.basename(name, '.js')
			.replace(/-/g, ':');

		loadTask(taskName, options);
	});
};