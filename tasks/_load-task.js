'use strict';

var gulp = require('gulp');

module.exports = function (name, options) {
	console.log('NAME: ' + name);

	var moduleName = name.replace(/:/g, '-');

	gulp.task(name, function (callback) {
		return require('./' + moduleName)(name, options, callback);
	});
};