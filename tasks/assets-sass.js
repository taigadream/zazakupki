'use strict';

var gulp = require('gulp'),
	gulpIf = require('gulp-if'),
	sass = require('gulp-sass'),
	clean = require('gulp-clean-css'),
	autoprefixer = require('gulp-autoprefixer'),
	gcmq = require('gulp-group-css-media-queries'),
	sourcemaps = require('gulp-sourcemaps'),
	rename = require('gulp-rename'),
	errorHandler = require('./_error-handler');

module.exports = function (name, options) {
	return gulp.src('src/assets/sass/main.scss')
		.pipe(errorHandler(name))
		.pipe(gulpIf(options.isDev, sourcemaps.init()))
		.pipe(sass())
		.pipe(autoprefixer({ browsers: ['last 2 versions', '> 5%'] }))
		.pipe(gulpIf(options.isProd, clean()))
		// .pipe(gulpIf(options.isProd, gcmq()))
		.pipe(gcmq())
		.pipe(rename('style.css'))
		.pipe(gulpIf(options.isDev, sourcemaps.write()))
		.pipe(gulp.dest('build/assets/css'));
};